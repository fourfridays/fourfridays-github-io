---
layout: page
permalink: /membership-payment/
---

Your annual membership payment helps for the maintenance and upkeep of the Islamic Center.

Please use one of the following options to pay your membership dues.

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="L5AMGFHX7ZEVJ">
<table>
<tr><td><input type="hidden" name="on0" value="Annual Membership"><strong>Annual Membership</strong></td></tr><tr><td><select name="os0">
	<option value="Family">Family $150.00 USD</option>
	<option value="Individual">Individual $75.00 USD</option>
</select> </td></tr>
</table>
<input type="hidden" name="currency_code" value="USD">
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynow_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>

