---
layout: page
title: Events
permalink: /events/
---
<!--
<strong>Eid</strong><br>
ISNA has officially announced: "Following its adopted current criteria for Eidul Adha, the Fiqh Council of North America announces that the final determination of the date of Eidul Adha will be <strong>Monday, September 12, 2016</strong>".

Thus, we will be observing fasting on the day of Arafat on Sunday, September 11th. Community Iftar in the form of a potluck will be held at the Masjid. 

Eid prayer will be held on Monday, September 12th, at 8:00AM in the Masjid.  Sisters will be using the second floor. A potluck light breakfast will be held IMMEDIATELY after prayer as usual.

عيد مبارك و كل عام و أنتم بخير. تقبل الله منا و منكم صالح الأعمال.
-->

<strong>Monthly dinner</strong> is last Saturday of every month after Maghreb prayers.

<strong>Islamic school</strong> for boys and girls start after Maghreb prayers every Friday.

<strong>Tea party and study circle</strong> after Isha prayers every Friday.
