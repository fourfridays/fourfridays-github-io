---
layout: page
title: Fundraising
permalink: /fundraising/
---

The Islamic Center of Emporia (ICEK) is pleased to invite you to our final phase project to remodel the third floor. As you may know, the ICEK’s membership is no longer sufficient to cover the monthly expenses of the masjid. These expenses cost $7,000 a month and include: water, electricity, gas, phone, and insurance. Thus, the executive committee of the masjid has decided to raise funds to remodel the third floor and secure a modest income to cover the monthly expenses. Thus, as members of an Islamic community and the only one in Emporia, we must seize this golden opportunity to give our perpetual charity (sadakah jariya) to support the Islamic center.

Our goal is to raise $40,000 by the end of the year.

All donations are gathered under the name of the Islamic Center of Emporia. Please donate by cash, check payable to Islamic Center of Emporia, or online.

Your donations are tax-deductible and a reciept can be provided upon request.

<!-- 16:9 aspect ratio -->
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/-k_8FPPGOf4"></iframe>
</div>

### How to Donate Online كيف ترسل تبرعاتك عبر النت؟
<a href="/docs/fundraising-donation-invitation-in-arabic.pdf" title="Fundraising Donation Invitation in Arabic"><button class="btn btn-primary">Donation Instructions in Arabic إضغط هنا</button></a>
